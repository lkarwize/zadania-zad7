from django.conf.urls import patterns, url, include
#from django.views.generic import TemplateView
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.contrib import admin
admin.autodiscover()#wykrywa modele



#urlpatterns = patterns('',
 #   url(r'^$', TemplateView.as_view(template_name='microblog/index.html'))
#)
from django.conf.urls import patterns, url
from django.http import HttpResponse
from django.views.generic import ListView, DetailView

from models import Wpis
import views


urlpatterns = patterns('',

    url(r'^blog/$', 'microblog.views.Articles'),
    url(r'^blog/get/(?P<wpis_id>\d+)/$', 'microblog.views.Article'),
    url(r'^blog/filter/$', 'microblog.views.Articles_filter'),
    url(r'^blog/create/$', 'microblog.views.create'),
    url(r'^blad/$', 'microblog.views.blad'),
    url(r'^admin/', include(admin.site.urls)),
    #urle do logowania/ rejestrowania sie
    url(r'^accounts/login/$', 'mysite.views.login' ),
    url(r'^accounts/auth/$', 'mysite.views.auth_view' ),
    url(r'^accounts/logout/$', 'mysite.views.logout' ),
    url(r'^accounts/loggedin/$', 'mysite.views.loggedin' ),
    url(r'^accounts/invalid/$', 'mysite.views.invalid_login' ),
    url(r'^accounts/register/$', 'mysite.views.register_user' ),
    url(r'^accounts/register_success/$', 'mysite.views.register_success' ),
    url(r'^blog/\S+/$', 'microblog.views.blad'),
    url(r'^accounts/\S+/$', 'microblog.views.blad'),
    url(r'^\S+/$', 'microblog.views.blad'),


)