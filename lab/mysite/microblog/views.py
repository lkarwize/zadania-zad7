from django.shortcuts import render_to_response
from form import WpisForm
from models import Wpis
from django.http import HttpResponseRedirect
from django.core.context_processors import  csrf
from django.contrib.auth.decorators import login_required

from django.template import RequestContext

@login_required(login_url='/~p16/mysite.wsgi/accounts/login/')
def create(request):
    podpisik = Wpis(podpis = request.user.username)
    if request.POST:
        form = WpisForm(request.POST, instance=podpisik,)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/~p16/mysite.wsgi/blog')

        else:
            form = WpisForm()
            args = {}
            args.update(csrf(request))
            args['wiad'] = "Zle wypelniles formularz"
            args['form'] = form
            return render_to_response('create_article.html', args,
                                context_instance=RequestContext(request),)
    else:
        form = WpisForm()
        args = {}
        args.update(csrf(request))

        args['form'] = form
        return render_to_response('create_article.html', args,
                                context_instance=RequestContext(request),)


def Articles(request):
    return render_to_response('index.html',
        {'articles' : Wpis.objects.all()},
        context_instance=RequestContext(request),)

def Articles_filter(request):
    return render_to_response('index.html',
        {'articles' : Wpis.objects.filter(podpis = request.user.username)},
        context_instance=RequestContext(request),)

def Article(request, wpis_id = 1):
    try:
        return render_to_response('article.html',
            {'wpis' : Wpis.objects.get(id = wpis_id ) },
                    context_instance=RequestContext(request),)
    except:
        return HttpResponseRedirect('/~p16/mysite.wsgi/blad')

def blad(request):
    return render_to_response('error.html',
                            context_instance=RequestContext(request),)